**Face Recognition**

Face Recognition is also described as a Biometric Artificial Intelligence based application that can uniquely identify a person by analyzing patterns based on the person's facial textures and shape.

A facial recognition system uses biometrics to map facial features from a photograph or video. It compares the information with a database of known faces to find a match.

Facial recognition can help verify personal identity, but it also raises privacy issues.

A facial recognition system works just like how humans identify faces, however on a grand, algorithmic scale.
Where humans need faces for recognition, computer needs data.

Here's a basic step-wise architecture required for face recognition:

![](https://www.pyimagesearch.com/wp-content/uploads/2018/09/opencv_face_reco_facenet-768x499.jpg)

1. A picture of your face is captured from a photo or video. Your face might appear alone or in a crowd.

2. Facial recognition software reads the geometry of your face. Key factors include the distance between your eyes and the distance from forehead to chin. The software identifies facial landmarks that are key to distinguishing your face. The result: your facial signature.

3. Your facial signature — a mathematical formula — is compared to a database of known faces.(Database consist million faces.)

4. A determination is made. Your faceprint may match that of an image in a facial recognition system database.

There are various methods that can be used for recognising faces.
In most of the cases developers use a very popular tool called **OpenCV**, in python, for facial detection and recognition.

OpenCV uses following methods,
- EigenFaces Face Recognizer
- FisherFaces Recognizer
- Local Binary Patterns Histograms


Face recognition has always come in handy while recognising criminals and fraudulent people,
and will remain important for security.
So has it's developement been one of the priorities.
